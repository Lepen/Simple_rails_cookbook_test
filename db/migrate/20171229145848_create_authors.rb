class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.integer :age
      t.datetime :birth_date
      t.boolean :famous, default: false
      
      t.timestamps
    end
  end
end
