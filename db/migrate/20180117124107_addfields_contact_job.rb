class AddfieldsContactJob < ActiveRecord::Migration[5.1]
  def change
    add_reference :contacts, :job, foreign_key: true
  end
end
