class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.integer :phone_number
      t.string :email
      t.string :country
      t.string :adress
      t.integer :door_number
      t.integer :fax
      t.integer :postal_code
      t.integer :mobile
      t.date :registered_date

      t.timestamps
    end
  end
end
