class CreateBannedAuthorsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :banned_authors_tables do |t|
      t.string :name
      t.datetime :bannedsince
      t.string :resson

      t.timestamps
    end
  end
end
