class AddfieldsRecipeTitlePhoneAuthorDate < ActiveRecord::Migration[5.1]
  def change
    add_column :recipes, :title, :string
    add_column :recipes, :phone, :integer
    add_column :recipes, :author, :string
    add_column :recipes, :date, :date
  end
end
