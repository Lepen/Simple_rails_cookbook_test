Rails.application.routes.draw do
  resources :jobs
  resources :contacts
  resources :authors
  resources :recipes 
  resources :bans
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index' 
end
