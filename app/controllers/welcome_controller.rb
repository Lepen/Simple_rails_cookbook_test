class WelcomeController < ApplicationController
  def index
    @authors_count = Author.count
    @recipes_count = Recipe.count
    @jobs_count = Job.count
    @contacts_count = Contact.count
  end
end

