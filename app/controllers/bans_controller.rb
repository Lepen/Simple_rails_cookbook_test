class BansController < ApplicationController
before_action :set_ban, only: [:ban]

# GET /recipes
# GET /recipes.json
def index
  @bans = ban.all
end

def banned
  @banned = banned.all
end 

# GET /recipes/1
# GET /recipes/1.json
def show
  @bans = ban.all
end


private
  def set_ban
    @ban = Ban.find(params[:id])
  end

  def ban_params
    params.require(:recipe).permit(:name, :bannedsince, :resson,)
  end
end