json.extract! author, :id, :first_name, :last_name, :gender, :age, :birth_date, :famous, :created_at, :updated_at
json.url author_url(author, format: :json)
