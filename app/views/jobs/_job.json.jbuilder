json.extract! job, :id, :career_name, :created_at, :updated_at
json.url job_url(job, format: :json)
