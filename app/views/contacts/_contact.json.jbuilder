json.extract! contact, :id, :first_name, :last_name, :phone_number, :email, :country, :adress, :door_number, :fax, :postal_code, :mobile, :registered_date, :created_at, :updated_at
json.url contact_url(contact, format: :json)
